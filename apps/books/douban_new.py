﻿# -*- coding: utf-8 -*-

import sys
from bs4 import BeautifulSoup
import time
import random
#import pandas as pd
import requests
if sys.version_info.major >= 3:
    import urllib.request
else:
    import urllib2
#import pymongo
import re
import codecs


class Douban():
    def __init__(self):
        self.headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36' }

        if sys.version_info.major >= 3:
            self.__r_author = r'作者:?</span>(.*?)<br/>'
            self.__r_publisher = r'出版社:?</span>(.*?)<br/>'
            self.__r_subtitle = r'副标题:?</span>(.*?)<br/>'
            self.__r_translator = r'译者:?</span>(.*?)<br/>'
            self.__r_publish_date = r'出版年:?</span>(.*?)<br/>'
            self.__r_pages = r'页数:?</span>(.*?)<br/>'
            self.__r_price = r'定价:?</span>(.*?)<br/>'
            self.__r_binding = r'装帧:?</span>(.*?)<br/>'
            self.__r_series = r'丛书:?</span>(.*?)<br/>'
            self.__r_isbn = r'ISBN:?</span>(.*?)<br/>'
        else:
            self.__r_author = unicode(r'作者:?</span>(.*?)<br/>')
            self.__r_publisher = unicode(r'出版社:?</span>(.*?)<br/>')
            self.__r_subtitle = unicode(r'副标题:?</span>(.*?)<br/>')
            self.__r_translator = unicode(r'译者:?</span>(.*?)<br/>')
            self.__r_publish_date = unicode(r'出版年:?</span>(.*?)<br/>')
            self.__r_pages = unicode(r'页数:?</span>(.*?)<br/>')
            self.__r_price = unicode(r'定价:?</span>(.*?)<br/>')
            self.__r_binding = unicode(r'装帧:?</span>(.*?)<br/>')
            self.__r_series = unicode(r'丛书:?</span>(.*?)<br/>')
            self.__r_isbn = unicode(r'ISBN:?</span>(.*?)<br/>')
    
    def __to_unicode__(self, text):
        text = re.sub('\\\\n', '', text)
        text = re.sub('\\\\x(..)', lambda x:unichr(int(x.group(1), 16)), text)
        #text = re.sub('\\\\x(..)', lambda x:'\\u00%s' % x.group(1), text)
        text = re.sub('\\\\u(....)', lambda x:unichr(int(x.group(1), 16)), text)
        return(text)

    def get_book(self, isbn=""):
        book = {'isbn': isbn}
        html = self.__get_html(isbn=isbn)
        if html == -1:  # 无此书
            return -1
        soup = self.__get_soup(html=html)
        book['title'] = self.__get_Title(soup=soup)

        self.__info__ = self.__get_info(soup=soup)
        book['author'] = self.__get_Author(soup=soup)
        book['name'] = book['author']
        book['publisher'] = self.__get_Publisher(soup=soup)
        book['subtitle'] = self.__get_Subtitle(soup=soup)
        book['translator'] = self.__get_Translator(soup=soup)
        book['pubdate'] = self.__get_PublishDate(soup=soup)
        book['pages'] = self.__get_Pages(soup=soup)
        book['price'] = self.__get_Price(soup=soup)
        book['binding'] = self.__get_Binding(soup=soup)
        book['series'] = self.__get_Series(soup=soup)
        book['isbn'] = self.__get_ISBN(soup=soup)

        book['rating'] = self.__get_Rating(soup=soup)
        book['tags'] = self.__get_Tags(soup=soup)
        
        book['author-intro'] = self.__get_AuthorIntro(soup=soup)
        book['summary'] = self.__get_Intro(soup=soup)
       
        return book

    def __get_html(self, isbn=""):
        url = 'http://douban.com/isbn/%s/' % (isbn)
        if sys.version_info.major >= 3:
            request = urllib.request.Request(url, headers=self.headers)
        else:
            request = urllib2.Request(url, headers=self.headers)
        #request = requests(url, headers=self.headers)
        try:
            if sys.version_info.major >= 3:
                response = urllib.request.urlopen(request)
            else:
                response = urllib2.urlopen(request)
        except:
            return -1
        if sys.version_info.major >= 3:
            html = response.read().decode('utf-8')
        else:
            html = unicode(response.read().decode('utf-8'))
        return html

    def __get_soup(self, html=""):
        soup = BeautifulSoup(html, 'lxml', exclude_encodings='utf-8')
        return soup

    def __get_info(self, soup):
        if sys.version_info.major >= 3:
            return(str(soup.select('#info')).replace('\n', '').replace('\r', ''))
        else:
            select = self.__to_unicode__(unicode(soup.select('#info')))
            return(select.replace('\n', '').replace('\r', ''))

    def __get_Title(self, soup):
        if sys.version_info.major >= 3:
            soupSelect = str(soup.select('#wrapper > h1 > span'))
            soupTemp = self.__get_soup(soupSelect)
            return str(soupTemp.text).strip('[] \n\t')
        else:
            soupSelect = self.__to_unicode__(unicode(soup.select('#wrapper > h1 > span')))
            soupSelect = re.sub('^.*?<span .*?>(.*?)</span>.*?$', lambda x:x.group(1), soupSelect, flags=re.UNICODE)
            #print(soupSelect)
            return(soupSelect)

    def __get_Author(self, soup):
        ans = re.findall(self.__r_author, self.__info__, flags=re.UNICODE)
        if sys.version_info.major >= 3:
            ans = re.findall(r'.*?<a .*?>(.+?)</a>', str(ans), flags=re.UNICODE)
        else:
            ans = re.findall(r'.*?<a .*?>(.+?)</a>', unicode(ans), flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str.join('; ', ans).strip(' \r\n\t')
            else:
                return self.__to_unicode__(unicode.join(u'; ', ans)).strip(u' \r\n\t')

    def __get_Publisher(self, soup):
        ans = re.findall(self.__r_publisher, self.__info__, flags=re.UNICODE)
        if len(ans) == 0:
            return ''
        else:
            if sys.version_info.major >= 3:
                return str(re.sub(r'<a .*?>(.*?)</a>', r'\1', ans[0])).strip(' \r\n\t')
            else:
                return self.__to_unicode__(unicode(re.sub(r'<a .*?>(.*?)</a>', r'\1', ans[0]))).strip(u' \r\n\t')

    def __get_Subtitle(self, soup):
        ans = re.findall(self.__r_subtitle, self.__info__, flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str(ans[0]).strip('[] \r\n\t')
            else:
                return self.__to_unicode__(unicode(ans[0])).strip(u'[] \r\n\t')

    def __get_Translator(self, soup):
        ans = re.findall(self.__r_translator, self.__info__, flags=re.UNICODE)
        if sys.version_info.major >= 3:
            ans = re.findall(r'<a .*?>(.+?)</a>', str(ans), flags=re.UNICODE)
        else:
            ans = re.findall(r'<a .*?>(.+?)</a>', unicode(ans), flags=re.UNICODE)
        if ans == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str.join('; ', ans)
            else:
                return self.__to_unicode__(unicode.join(u'; ', ans))

    def __get_PublishDate(self, soup):
        ans = re.findall(self.__r_publish_date, self.__info__, flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str(ans[0]).strip('[] \r\n\t')
            else:
                return self.__to_unicode__(unicode(ans[0])).strip(u'[] \r\n\t')

    def __get_Pages(self, soup):
        ans = re.findall(self.__r_pages, self.__info__, flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str(ans[0]).strip('[] \r\n\t页')
            else:
                return unicode(ans[0]).strip(u'[] \r\n\t页')

    def __get_Price(self, soup):
        ans = re.findall(self.__r_price, self.__info__, flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str(ans[0]).strip('[] \r\n\t元')
            else:
                return unicode(ans[0]).strip(u'[] \r\n\t元')

    def __get_Binding(self, soup):
        ans = re.findall(self.__r_binding, self.__info__, flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str(ans[0]).strip("[] \r\n\t")
            else:
                return self.__to_unicode__(unicode(ans[0])).strip(u'[] \r\n\t')

    def __get_Series(self, soup):
        ans = re.findall(self.__r_series, self.__info__, flags=re.UNICODE)
        if sys.version_info.major >= 3:
            ans = re.findall(r'<a .*?>(.+?)</a>', str(ans))
        else:
            ans = re.findall(r'<a .*?>(.+?)</a>', unicode(ans), flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            if sys.version_info.major >= 3:
                return str(ans[0]).strip('[] \r\n\t')
            else:
                return self.__to_unicode__(unicode(ans[0])).strip(u'[] \r\n\t')

    def __get_ISBN(self, soup):
        ans = re.findall(self.__r_isbn, self.__info__, flags=re.UNICODE)
        if len(ans) == 0:
            return ""
        else:
            return str(ans[0]).strip("[] \r\n\t")

    def __get_Rating(self, soup):
        #interest_sectl > div > div.rating_self.clearfix > strong
        soupSelect = str(soup.select(".rating_num")[0].text).replace('\n', '')
        return str(soupSelect).strip("[] \n\t")

    def __get_AuthorIntro(self, soup):
        try:
            soupSelect = soup.select("div.related_info > div.indent")[1].select(".intro")
            if sys.version_info.major >= 3:
                soupSelect = re.findall(r'<p>(.*?)</p>', str(soupSelect[len(soupSelect)-1]))
                soupSelect = str.join('\n\r', soupSelect)
                return str(soupSelect).strip("[] \n\t")
            else:
                soupSelect = re.findall(r'<p>(.*?)</p>', unicode(soupSelect[len(soupSelect)-1]))
                soupSelect = unicode.join(u'\n\r', soupSelect)
                return unicode(soupSelect).strip("[] \n\t")
        except:
            return ""

    def __get_Intro(self, soup):
        try:
            soupSelect = soup.select("#link-report .intro")
            if sys.version_info.major >= 3:
                soupSelect = re.findall(r'<p>(.*?)</p>', str(soupSelect[len(soupSelect)-1]))
                soupSelect = str.join('\n\r', soupSelect)
                return str(soupSelect).strip("[] \n\t")
            else:
                soupSelect = re.findall(r'<p>(.*?)</p>', unicode(soupSelect[len(soupSelect)-1]))
                soupSelect = unicode.join(u'\n\r', soupSelect)
                return unicode(soupSelect).strip("[] \n\t")
        except:
            return ""

    def __get_Tags(self, soup):
        soupSelect = soup.select("#db-tags-section > div > span")
        soupSelect = map(lambda x:x.text.strip(), soup.select("#db-tags-section > div > span"))
        if sys.version_info.major >= 3:
            soupSelect = str.join('; ', soupSelect)
            return str(soupSelect).strip("[] \n\t")
        else:
            soupSelect = unicode.join(u'; ', soupSelect)
            return unicode(soupSelect).strip("[] \n\t")


if __name__ == "__main__":
    base = Douban()
    print("Query %s ......" % sys.argv[1])
    print(base.get_book(sys.argv[1]))
